var myApp = angular.module('myApp', []);

myApp.controller('mainController', function($scope) {
    $scope.hotels = [
		{
			name: 'Hilton',
			price: 300,
            active:false
		},{
			name: 'Crown Plaza',
			price: 400,
            active:false
		},{
			name: 'Intercontinental',
			price: 250,
            active:false
		},{
			name: 'Mandarin Oriental',
			price: 220,
            active:false
		}
	];
    
    $scope.toggleActive = function(s){
		s.active = !s.active;
	};

	// Helper method for calculating the total price

	$scope.total = function(){

		var total = 0;

		// Use the angular forEach helper method to
		// loop through the services array:

		angular.forEach($scope.hotels, function(s){
			if (s.active){
				total+= s.price;
			}
		});

		return total;
	};
    
    console.log($scope);
});